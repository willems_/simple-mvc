<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Page not found!</title>
	<link rel="stylesheet" href="/css/bulma.min.css">
</head>

<body>
	<section class="hero is-danger is-fullheight">
		<div class="hero-body">
			<div class="container has-text-centered">
				<h1 class="title is-1">404</h1>
				<h2 class="subtitle">Page not found!</h2>
			</div>
		</div>
	</section>
</body>

</html>