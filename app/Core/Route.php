<?php

class Route
{
	protected static $routes = [];
	protected static $url = '/';

	public static function get($url, $controller, $method)
	{
		self::$routes['GET'][$url] = ['controller' => $controller, 'method' => $method];
	}

	public static function post($url, $controller, $method)
	{
		self::$routes['POST'][$url] = ['controller' => $controller, 'method' => $method];
	}

	private static function controllerMethod($controller, $method)
	{
		if (file_exists('../app/Controllers/' . $controller . '.php')) {
			require_once '../app/Controllers/' . $controller . '.php';

			if (method_exists($controller, $method)) {
				$a = new $controller;
				$a->$method();
			}
		}
	}

	public static function redirect()
	{
		$url = isset($_GET['url']) ? $_GET['url'] : self::$url;
		$requestType = $_SERVER['REQUEST_METHOD'];

		if ($requestType == 'GET') {
			if (array_key_exists($url, self::$routes['GET'])) {
				return self::controllerMethod(self::$routes['GET'][$url]['controller'], self::$routes['GET'][$url]['method']);
			}
		} else if ($requestType == 'POST') {
			if (array_key_exists($url, self::$routes['POST'])) {
				return self::controllerMethod(self::$routes['POST'][$url]['controller'], self::$routes['POST'][$url]['method']);
			}
		}

		self::errorPage();
	}

	public static function errorPage()
	{
		$controller = new Controller();
			return $controller->view('404');
	}
}
