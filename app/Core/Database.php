<?php

class Database
{
	private static $host = 'remotemysql.com';
	private static $dbName = 'AEMiepS5oa';
	private static $username = 'AEMiepS5oa';
	private static $password = 'yHHKgZMSKJ';

	private static function connect()
	{
		try {
			return new PDO("mysql:host=" . self::$host . ";dbname=" . self::$dbName . ";", self::$username, self::$password, [
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			]);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public static function query($query, $params = [])
	{
		$stmt = self::connect()->prepare($query);
		$stmt->execute($params);

		// Check if statement starts with SELECT
		if (explode(' ', $query)[0] == 'SELECT') {
			return $stmt->fetchAll();
		}
	}
}
